import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import WeatherActual from '../models/weather-actual.model';
import WeatherForecast from '../models/weather-forecast.model';
import {Utils} from '../utils';

@Injectable({
  providedIn: 'root'
})
export class WeatherService {

  appKey = '69b8b0dcac2dd31883dd5beec51ec4e1';
  baseUrl = `https://api.openweathermap.org/data/2.5/`;
  city = 724443;
  // TODO implement cache

  constructor(private http: HttpClient) { }

  public getActual(): Observable<WeatherActual> {
    return this.http.get<WeatherActual>(this.getUrl('weather'));
  }

  /**
   * Get 3 hours forecast per 5 days
   * @returns {Observable<WeatherForecast>}
   */
  public getForecast(): Observable<WeatherForecast> {
    return this.http.get<WeatherForecast>(this.getUrl('forecast'));
  }

  private getUrl(service: string) {
    return `${this.baseUrl}${service}?id=${this.city}&APPID=${this.appKey}&units=metric`;
  }

  /**
   * Filter only days at 12:00, min and max sets depends on min/max temp during actual day
   * @param {WeatherForecast} forecast
   */
  public aggregateMinMaxTemps(forecast: WeatherForecast) {
    const days = forecast.list.filter(day => !Utils.isToday(day.dt) && day.dt_txt.indexOf('12:00') !== -1);


    return {...forecast, list: days} as WeatherForecast;
  }
}
