import { Injectable } from '@angular/core';
import {BehaviorSubject, Observable} from 'rxjs';
import Meta from '../models/meta.model';

const metaFake: Meta = {
  bg: {
    day: '',
    night: '',
    name: ''
  },
  opacity: 0.5,
  color: '0, 0, 0'
};

@Injectable({
  providedIn: 'root'
})
export class MetaService {
  private meta = new BehaviorSubject<Meta>(metaFake);

  constructor() { }

  public getMeta(): Observable<Meta> {
    return this.meta.asObservable();
  }

  public setMeta(meta: Meta): void {
    this.meta.next(meta);
    this.persistData();
  }

  public persistData() {
    localStorage.setItem('meta', JSON.stringify(this.meta.getValue()));
  }

  public loadData() {
    const meta = localStorage.getItem('meta');
    if (meta) {
      this.meta.next(JSON.parse(meta));
    }
  }
}
