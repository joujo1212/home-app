import { Injectable } from '@angular/core';
import {WidgetService} from './widget.service';
import {MetaService} from './meta.service';

@Injectable({
  providedIn: 'root'
})
export class StorageService {

  constructor(private widgetService: WidgetService, private metaService: MetaService) { }

  // public persistData() {
  //   localStorage.setItem('meta', JSON.stringify(this.meta.getValue()));
  //   localStorage.setItem('widgets', JSON.stringify(this.widgets.getValue()));
  // }
  //
  // public loadData() {
  //   const meta = localStorage.getItem('meta');
  //   if (meta) {
  //     this.metaService.setMeta(JSON.parse(meta));
  //   }
  //   const widgets = localStorage.getItem('widgets');
  //   if (widgets) {
  //     this.widgetService.setWidgets(JSON.parse(widgets));
  //   }
  // }
}
