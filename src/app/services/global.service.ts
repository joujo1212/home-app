import {Injectable} from '@angular/core';
import {BehaviorSubject, Observable, Subject} from 'rxjs';
import Command from '../models/command.model';

@Injectable({
  providedIn: 'root'
})
export class GlobalService {
  private commands = new Subject<Command>();
  private sideNavState = new BehaviorSubject<boolean>(false);

  constructor() {
  }

  public pushCommand(command: Command) {
    if (command) {
      this.commands.next(command);
    }
  }

  public getCommandsStream(): Observable<Command> {
    return this.commands.asObservable();
  }

  public setSideNavState(state: boolean) {
    if (state) {
      this.sideNavState.next(state);
    }
  }

  public getSideNavState(): Observable<boolean> {
    return this.sideNavState.asObservable();
  }
}
