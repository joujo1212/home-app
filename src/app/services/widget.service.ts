import { Injectable } from '@angular/core';
import {BehaviorSubject, Observable} from 'rxjs';
import Widget from '../models/widget.model';

const widgetsFake: Widget[] = [
  {
    id: 'forecast',
    x: 0,
    y: 0,
    cols: 2,
    rows: 4,
    component: 'forecast',
    visible: false
  },
  {
    id: 'actualWeather',
    x: 1,
    y: 0,
    cols: 2,
    rows: 4,
    component: 'weather',
    visible: false
  },
  {
    id: 'voice',
    x: 2,
    y: 0,
    cols: 2,
    rows: 4,
    component: 'voice',
    visible: false
  },
  {
    id: 'shoppingList',
    x: 0,
    y: 0,
    cols: 2,
    rows: 4,
    component: 'shoppingList',
    visible: false
  },
  {
    id: 'stopwatch',
    x: 0,
    y: 0,
    cols: 2,
    rows: 4,
    component: 'stopwatch',
    visible: false
  },
  {
    id: 'countdown',
    x: 0,
    y: 0,
    cols: 2,
    rows: 4,
    component: 'countdown',
    visible: false
  },
  {
    id: 'clock',
    x: 0,
    y: 5,
    cols: 2,
    rows: 4,
    component: 'clock',
    visible: false
  },
  {
    id: 'alarm',
    x: 0,
    y: 5,
    cols: 4,
    rows: 4,
    component: 'alarm',
    visible: false
  }
];
@Injectable({
  providedIn: 'root'
})
export class WidgetService {
  private widgets = new BehaviorSubject<Widget[]>(widgetsFake);

  constructor() { }

  public getWidgets(): Observable<Widget[]> {
    return this.widgets.asObservable();
  }

  public setWidgets(widgets): void {
    this.widgets.next(widgets);
    this.persistData();
  }

  /**
   *
   * @param {string} id unique ID of widget type
   * @param {{}} data any data which want to update/add to the widget
   */
  public updateWidget(id: string, data: {}) {
    if (!id) { return; }
    let foundWidget = this.widgets.getValue().find(widget => widget.id === id);
    if (!foundWidget) { return; }
    foundWidget = Object.assign(foundWidget, data);
    this.widgets.next(this.widgets.getValue());
    this.persistData();
  }

  public getWidget(id: string) {
    return this.widgets.getValue()[id];
  }

  public persistData() {
    localStorage.setItem('widgets', JSON.stringify(this.widgets.getValue()));
  }

  public loadData() {
    const widgets = localStorage.getItem('widgets');
    if (widgets) {
      let loadedWidgets: Widget[] = JSON.parse(widgets);
      const newWidgets = widgetsFake.filter(widget => !loadedWidgets.some(loadedWidget => loadedWidget.id === widget.id));
      // if some widget is not in local storage, inject it for ability to show this widget
      loadedWidgets = [...loadedWidgets, ...newWidgets];

      this.widgets.next(loadedWidgets);
    }
  }
}
