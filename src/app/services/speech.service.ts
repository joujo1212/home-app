import {Injectable} from '@angular/core';
import {BehaviorSubject, Observable} from 'rxjs';
import PNotify from 'pnotify/dist/es/PNotify';

const synth = window.speechSynthesis;
declare var webkitSpeechRecognition: any;
declare var webkitSpeechGrammarList: any;

const SpeechRecognition = webkitSpeechRecognition;
const SpeechGrammarList = webkitSpeechGrammarList;
// const SpeechRecognition = SpeechRecognition || webkitSpeechRecognition;
// const SpeechGrammarList = SpeechGrammarList || webkitSpeechGrammarList;

// TODO investigate how it works
const colors = ['aqua', 'azure', 'beige', 'bisque', 'black', 'blue', 'brown', 'chocolate', 'coral', 'crimson', 'cyan', 'fuchsia', 'ghostwhite', 'gold', 'goldenrod', 'gray', 'green', 'indigo', 'ivory', 'khaki', 'lavender', 'lime', 'linen', 'magenta', 'maroon', 'moccasin', 'navy', 'olive', 'orange', 'orchid', 'peru', 'pink', 'plum', 'purple', 'red', 'salmon', 'sienna', 'silver', 'snow', 'tan', 'teal', 'thistle', 'tomato', 'turquoise', 'violet', 'white', 'yellow'];
const grammar = '#JSGF V1.0; grammar colors; public <color> = ' + colors.join(' | ') + ' ;';

@Injectable({
  providedIn: 'root'
})
export class SpeechService {
  voices = [];
  /**
   * True if system is speaking something
   * @type {boolean}
   */
  private isSpeaking = false;
  private recognition;
  private speechRecognitionList = new SpeechGrammarList();
  private isListening = new BehaviorSubject<boolean>(false);

  constructor() {
    setTimeout(() => {
      this.recognition = new SpeechRecognition();
      this.initRecognition();
    }, 5000);
  }

  private populateVoiceList() {
    this.voices = window.speechSynthesis.getVoices();
    console.log(this.voices);
  }

  private initRecognition() {
    this.speechRecognitionList.addFromString(grammar, 1);
    this.recognition.grammars = this.speechRecognitionList;
    this.recognition.lang = 'sk';
    this.recognition.maxAlternatives = 1;
  }

  private setIsSpeaking(isSpeaking: boolean) {
    this.isSpeaking = isSpeaking;
    console.log('isSpeaking', isSpeaking);
    if (isSpeaking) {
      this.recognition.abort();
      this.isListening.next(false);
    } else {
      try {
      this.recognition.abort();
      this.isListening.next(false);
      setTimeout(() => {
        this.recognition.start();
        this.isListening.next(true);
        console.log('start listening');
      }, 1000);
      } catch {}
    }
  }

  public speak(text: string): Promise<void> {
    this.showNotif(text);
    // update voices list each time
    this.populateVoiceList();
    // cancel all speaks and start new one
    synth.cancel();

    this.isSpeaking = true;
    this.setIsSpeaking(true);
    const o = new Promise<void>((resolve, reject) => {

      if (synth.speaking) {
        console.error('speechSynthesis.speaking');
        // this.setIsSpeaking(false);
        return;
      }
      const utterThis = new SpeechSynthesisUtterance(text);
      utterThis.onend = event => {
        console.log('SpeechSynthesisUtterance.onend');
        this.setIsSpeaking(false);
        resolve();
      };
      utterThis.onerror = event => {
        console.error('SpeechSynthesisUtterance.onerror');
      };
      utterThis.voice = this.voices[19];
      synth.speak(utterThis);
    });

    return o;
  }

  private showNotif(title: string) {
    PNotify.defaults.delay = 1000;
    if (typeof (<any>window).stackBottomLeft === 'undefined') {
      (<any>window).stackBottomLeft = {
        'dir1': 'up',
        'firstpos1': 25,
        'push': 'top'
      };
    }
    PNotify.notice({
      title,
      type: 'info',
      width: '16vw',
      icon: 'fa fa-microphone',
      styling: {},
      addClass: 'custom nonblock',

      // text: 'My buttons are really lonely, so they\'re gonna hang out with us.',
      modules: {
        Animate: {
          animate: true,
          inClass: 'fadeInUp',
          outClass: 'fadeOutUp'
        }
      },
      stack: (<any>window).stackBottomLeft

    });
  }

  observer;
  public listen(continuousListening = false): Observable<string> {
    this.recognition.interimResults = true;
    this.recognition.continuous = true;
    console.log('listen');
    const o = new Observable<string>((observer) => {
      this.observer = observer;
      this.recognition.abort();
      this.isListening.next(false);
      console.log('stop listening');
      setTimeout(() => {
        this.recognition.start();
        this.isListening.next(true);
        console.log('start listening');
      }, 1000);
      // this.recognition.start();
      console.log('Ready to receive a color command.');

      this.recognition.onresult = event => {
        // console.log('Confidence: ' + event.results[0][0].confidence);

        let allTexts = '';
        Object.keys(event.results).forEach(key => { allTexts += event.results[key][0].transcript; });
        console.log(allTexts);
        observer.next(allTexts);
        // observer.complete();
      };

      this.recognition.onspeechend = () => {
        // this.recognition.stop();
      };

      this.recognition.onnomatch = event => {
        console.log('I didn\'t recognise that color.');
      };

      this.recognition.onerror = event => {
        console.log('Error occurred in recognition: ' + event.error);
      };

      this.recognition.onend = () => {
        console.log('Listening on end ');
        // this.isListening.next(false);
        // TODO risky needs to investigate why listening is ended randomly
        this.recognition.start();
      };
    });

    return o;
  }

  public stopListening() {
    console.log('stop listening');
    this.observer.complete();
    this.recognition.abort();
    this.isListening.next(false);
  }
  public processInput(text: string): Array<string> {
    return text
      .trim()
      .toLowerCase()
      .replace(',', ' ')
      .replace('.', '')
      .replace('  ', ' ')
      .split(' ');
  }

  public setIsListening(isListening: boolean) {
    this.isListening.next(isListening);
  }

  public getIsListening(): Observable<boolean> {
    return this.isListening.asObservable();
  }
}
