import {Component, OnInit} from '@angular/core';
import M from 'materialize-css/dist/js/materialize';
import 'pnotify/dist/es/PNotifyAnimate';
import {MetaService} from './services/meta.service';
import {WidgetService} from './services/widget.service';

// declare const Packery: any;

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  public focused = -1;

  constructor(private metaService: MetaService, private widgetService: WidgetService) {}

  ngOnInit() {
    M.AutoInit();
    this.metaService.loadData();
    this.widgetService.loadData();
  }

  public focus(n) {
    if (this.focused === n) {
      this.focused = -1;
    } else {
      this.focused = n;
    }
  }
}
