export default interface ShoppingListItem {
  name: string;
  done: boolean;
}
