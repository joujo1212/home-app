import {GridsterItem} from 'angular-gridster2';

export default interface Widget extends GridsterItem {
  // TODO is ID neccessary if component field exists ???
  id: string;
  // name of widget, e.g. 'forecast'
  component: string;
  // true if widget should be initialized on dashboard, else for completely remove from dashboard
  visible: boolean;
}
