export default class Command {
  type: string;
  args?: string | Array<any>;

  constructor(type: string, args?: string | Array<any> | any) {
    this.type = type;
    this.args = args;
  }
}
