import Background from './background.model';

export default interface Meta {
  bg: Background;
  // 0 - 1
  opacity: number;
  // format: R, G, B
  color: string;
}
