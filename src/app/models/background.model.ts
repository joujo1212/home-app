export default interface Background {
  day: string;
  night: string;
  name: string;
}
