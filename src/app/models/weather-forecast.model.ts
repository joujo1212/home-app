export default interface WeatherForecast {
  cod: number;
  message: number;
  cnt: number;
  list: [
    {
      dt: number,
      dt_txt: string,
      main: {
        temp: number,
        pressure: number,
        humidity: number,
        temp_min: number,
        temp_max: number,
        temp_kf: number,
        grnd_level: number,
        sea_level: number,
      },
      weather: [
        {
          id: number,
          main: string,
          description: string,
          icon: string
        }
      ],
      clouds: {
        all: number,
        // FE field
        image: string
      },
      wind: { speed: number, deg: number },
      rain: { any },
      sys: { pod: string }
    }
  ];
  city: {
    id: number,
    name: string,
    coord: {
      lat: number,
      lon: number
    },
    country: string
  };
}
