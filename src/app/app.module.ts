import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import {SpeechService} from './services/speech.service';
import { BgComponent } from './components/bg/bg.component';
import { VoiceComponent } from './widgets/voice/voice.component';
import {WeatherService} from './services/weather.service';
import {HttpClientModule} from '@angular/common/http';
import { WeatherComponent } from './widgets/weather/weather.component';
import {GlobalService} from './services/global.service';
import { SpinnerComponent } from './components/spinner/spinner.component';
import { ShoppingListComponent } from './widgets/shopping-list/shopping-list.component';
import {FormsModule} from '@angular/forms';
import { GridsterModule } from 'angular-gridster2';
import { DashboardComponent } from './components/dashboard/dashboard.component';
import { WidgetComponent } from './components/widget/widget.component';
import { ForecastComponent } from './widgets/forecast/forecast.component';
import { SidenavComponent } from './components/sidenav/sidenav.component';
import { StopwatchComponent } from './widgets/stopwatch/stopwatch.component';
import { CountdownComponent } from './widgets/countdown/countdown.component';
import {MetaService} from './services/meta.service';
import {WidgetService} from './services/widget.service';
import { DynamicModule } from 'ng-dynamic-component';
import { SafePipe } from './pipes/safe.pipe';
import { ClockComponent } from './widgets/clock/clock.component';
import { AlarmComponent } from './widgets/alarm/alarm.component';

@NgModule({
  declarations: [
    AppComponent,
    BgComponent,
    SpinnerComponent,
    DashboardComponent,
    WidgetComponent,
    SidenavComponent,
    // Widgets
    VoiceComponent,
    WeatherComponent,
    ShoppingListComponent,
    ForecastComponent,
    StopwatchComponent,
    CountdownComponent,
    SafePipe,
    ClockComponent,
    AlarmComponent
  ],
  entryComponents: [
    VoiceComponent,
    WeatherComponent,
    ShoppingListComponent,
    ForecastComponent,
    StopwatchComponent,
    CountdownComponent,
    ClockComponent,
    AlarmComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpClientModule,
    GridsterModule,
    DynamicModule.withComponents([
      VoiceComponent,
      WeatherComponent,
      ShoppingListComponent,
      ForecastComponent,
      StopwatchComponent,
      CountdownComponent,
      ClockComponent,
      AlarmComponent
    ])
  ],
  providers: [SpeechService, WeatherService, GlobalService, MetaService, WidgetService],
  bootstrap: [AppComponent]
})
export class AppModule { }
