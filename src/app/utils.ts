import * as moment from 'moment';

/* Get the documentElement (<html>) to display the page in fullscreen */

const elem = document.documentElement;

export class Utils {
  /* View in fullscreen */
  static openFullscreen() {
    if (elem.requestFullscreen) {
      elem.requestFullscreen();
    } else if ((<any>elem).mozRequestFullScreen) { /* Firefox */
      (<any>elem).mozRequestFullScreen();
    } else if ((<any>elem).webkitRequestFullscreen) { /* Chrome, Safari and Opera */
      elem.webkitRequestFullscreen();
    } else if ((<any>elem).msRequestFullscreen) { /* IE/Edge */
      (<any>elem).msRequestFullscreen();
    }
  }

  /* Close fullscreen */
  static closeFullscreen() {
    if (document.exitFullscreen) {
      document.exitFullscreen();
    } else if ((<any>document).mozCancelFullScreen) { /* Firefox */
      (<any>document).mozCancelFullScreen();
    } else if (document.webkitExitFullscreen) { /* Chrome, Safari and Opera */
      document.webkitExitFullscreen();
    } else if ((<any>document).msExitFullscreen) { /* IE/Edge */
      (<any>document).msExitFullscreen();
    }
  }

  static cloudyTranslate(cloudyPerc: number): string {
    if (cloudyPerc >= 75) {
      return 'zamračené';
    }
    if (cloudyPerc >= 50) {
      return 'polooblačno';
    }
    if (cloudyPerc >= 25) {
      return 'polojasno';
    }
    if (cloudyPerc >= 0) {
      return 'jasno';
    }
  }

  static cloudyImage(cloudyPerc: number): string {
    if (cloudyPerc >= 75) {
      return 'assets/weather_icons/static/cloudy.svg';
    }
    if (cloudyPerc >= 50) {
      return 'assets/weather_icons/static/cloudy-day-3.svg';
    }
    if (cloudyPerc >= 25) {
      return 'assets/weather_icons/static/cloudy-day-1.svg';
    }
    if (cloudyPerc >= 0) {
      return 'assets/weather_icons/static/day.svg';
    }
  }

  static formatDate(format: string, date = Date.now()): string {
    return moment(date).format(format);
  }

  static isToday(date): boolean {
    return moment().diff(date, 'days') === 0;
  }

  static addTime(hours: number, minutes: number, seconds: number, millis = 0, date = Date.now()): number {
    return moment(date)
      .add(hours, 'hour')
      .add(minutes, 'minute')
      .add(seconds, 'second')
      .add(millis, 'ms')
      .toDate().getTime();
  }

  static addDay(days: number, date = Date.now()): Date {
    return moment(date)
      .add(days, 'day')
      .toDate();
  }

  static parseDateFromString(text: string, format: string) {
    return moment(text, format).toDate();
  }
}
