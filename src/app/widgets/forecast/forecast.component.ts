import { Component, OnInit } from '@angular/core';
import {Utils} from '../../utils';
import WeatherForecast from '../../models/weather-forecast.model';
import {GlobalService} from '../../services/global.service';
import {WeatherService} from '../../services/weather.service';
import Command from '../../models/command.model';
import {SpeechService} from '../../services/speech.service';

@Component({
  selector: 'app-forecast',
  templateUrl: './forecast.component.html',
  styleUrls: ['./forecast.component.scss']
})
export class ForecastComponent implements OnInit {

  public forecast: WeatherForecast;

  constructor(private weatherService: WeatherService, private speechService: SpeechService, private globalService: GlobalService) {
  }

  ngOnInit() {
    this.weatherService.getForecast().subscribe((data: WeatherForecast) => {
      this.forecast = this.weatherService.aggregateMinMaxTemps(data);
      this.forecast.list.forEach(day => {
        day.dt_txt = this.formatDate(day.dt * 1000);
        day.main.temp = this.roundTemp(day.main.temp);
        day.wind.speed = this.roundTemp(day.wind.speed);
        day.clouds.image = this.cloudyImage(day.clouds.all);
      });
      console.log(data);
    });
    this.globalService.getCommandsStream().subscribe((command: Command) => {
      switch (command.type) {
        case 'forecast_weather':
          this.handleForecastWeather();
          break;
        default:
      }
    });
  }

  handleForecastWeather() {
    this.weatherService.getForecast().subscribe((data: WeatherForecast) => {
      this.speechService.speak(`Zajtra na obed bude ${this.roundTemp(data.list[12].main.temp)} stupňov.`).then(() => {
        this.speechService.speak(`Pozajtra na obed bude ${this.roundTemp(data.list[20].main.temp)} stupňov.`);
      });
    });
  }

  roundTemp(temp: number) {
    console.log('aa');
    return Math.round(temp);
  }

  public cloudyImage(cloudyPerc: number) {
    console.log('cc');
    return Utils.cloudyImage(cloudyPerc);
  }

  public formatDate(date) {
    console.log('bb');
    return Utils.formatDate('ddd', date);
  }

}
