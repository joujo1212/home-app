import {Component, OnDestroy, OnInit} from '@angular/core';
import {Utils} from '../../utils';
import Command from '../../models/command.model';
import {GlobalService} from '../../services/global.service';
import {SpeechService} from '../../services/speech.service';
import {Subscription} from 'rxjs';

@Component({
  selector: 'app-stopwatch',
  templateUrl: './stopwatch.component.html',
  styleUrls: ['./stopwatch.component.scss']
})
export class StopwatchComponent implements OnInit, OnDestroy {

  // private stopwatch = {
  //   name: 'Stopwatch 1',
  //   time: null,
  //   type: 'stopwatch'
  // };
  public intervalID;
  public time = '00:00:00.00';
  public startTime;
  private overallTime = 0;
  private globalSub: Subscription;

  constructor(private globalService: GlobalService, private speechService: SpeechService) { }

  ngOnInit() {
    this.globalSub = this.globalService.getCommandsStream().subscribe((command: Command) => {
      switch (command.type) {
        case 'start':
          this.start(true);
          // TODO stop listening
          this.speechService.speak('Štart');
          break;
        case 'stop':
          // TODO stop listening
          this.speechService.speak(`Stop. Čas: ${(((Date.now() - this.startTime) / 1000) + '').replace('.', ',')} sekúnd`);
          this.stop();
          break;
        default:
      }
    });
  }

  ngOnDestroy() {
    this.stop();
    this.globalSub.unsubscribe();
  }

  public start(reset?: boolean) {
    console.log('start');
    if (reset) {
      this.stop();
    }
    this.startTime = Date.now();
    this.startInterval();
  }

  private startInterval() {
    this.intervalID = setInterval(() => {
      const diff = Date.now() - this.startTime;
      this.time = this.format(this.overallTime + diff);
    }, 40);
  }

  public stop() {
    console.log('stop');
    clearInterval(this.intervalID);
    this.intervalID = null;
    this.startTime = null;
    this.overallTime = 0;
  }

  public pause() {
    this.overallTime += Date.now() - this.startTime;
    clearInterval(this.intervalID);
    this.intervalID = null;
  }

  private format(timestamp) {
    return Utils.formatDate('HH:mm:ss.SS', timestamp);
  }
}
