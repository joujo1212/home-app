import {Component, OnDestroy, OnInit} from '@angular/core';
import {Utils} from '../../utils';

@Component({
  selector: 'app-clock',
  templateUrl: './clock.component.html',
  styleUrls: ['./clock.component.scss']
})
export class ClockComponent implements OnInit, OnDestroy {
  private intervalID;
  public hours = '00';
  public minutes = '00';
  public date = '';
  public dots = true;

  ngOnInit() {
    this.render();
    this.startInterval();
  }

  ngOnDestroy() {
    this.stop();
  }

  private startInterval() {
    this.intervalID = setInterval(() => {
      this.render();
    }, 1000);
  }

  private stop() {
    clearInterval(this.intervalID);
    this.intervalID = null;
  }

  private render() {
    const now = Date.now();
    this.hours = Utils.formatDate('HH', now);
    this.minutes = Utils.formatDate('mm', now);
    this.date = new Date().toLocaleDateString();
    this.dots = !this.dots;
  }
}
