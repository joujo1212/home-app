import {Component, OnInit} from '@angular/core';
import WeatherActual from '../../models/weather-actual.model';
import {WeatherService} from '../../services/weather.service';
import {SpeechService} from '../../services/speech.service';
import {GlobalService} from '../../services/global.service';
import WeatherForecast from '../../models/weather-forecast.model';
import Command from '../../models/command.model';
import {Utils} from '../../utils';

@Component({
  selector: 'app-weather',
  templateUrl: './weather.component.html',
  styleUrls: ['./weather.component.scss']
})
export class WeatherComponent implements OnInit {

  public actualWeather: WeatherActual;

  constructor(private weatherService: WeatherService, private speechService: SpeechService, private globalService: GlobalService) {
  }

  ngOnInit() {
    this.weatherService.getActual().subscribe((data: WeatherActual) => {
      this.actualWeather = data;
    });
    this.globalService.getCommandsStream().subscribe((command: Command) => {
      switch (command.type) {
        case 'actual_weather':
          this.handleActualWeather();
          break;
        default:
      }
    });
  }


  handleActualWeather() {
    this.weatherService.getActual().subscribe((data: WeatherActual) => {
      this.actualWeather = data;
      this.speechService.speak(`Aktuálne je vonku ${this.roundTemp(data.main.temp)} stupňov.`);
    });
  }

  roundTemp(temp: number) {
    return Math.round(temp);
  }

  public cloudyImage(cloudyPerc: number) {
    return Utils.cloudyImage(cloudyPerc);
  }
}
