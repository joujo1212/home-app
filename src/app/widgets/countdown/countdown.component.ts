import {AfterViewInit, Component, OnDestroy, OnInit, ViewChild} from '@angular/core';
import {Utils} from '../../utils';
import {GlobalService} from '../../services/global.service';
import Command from '../../models/command.model';
import Picker from 'pickerjs/dist/picker.esm';
import {Subscription} from 'rxjs';

@Component({
  selector: 'app-countdown',
  templateUrl: './countdown.component.html',
  styleUrls: ['./countdown.component.scss']
})
export class CountdownComponent implements OnInit, OnDestroy, AfterViewInit {
  @ViewChild('picker') picker;
  public intervalID;
  public time = '00:00:00.00';
  public startTime;
  private overallTime = 0;
  public desiredTime = {
    h: 0,
    m: 3,
    s: 0
  };
  public endTime;
  // true if time arrived
  public arrived = false;
  private alarmSound = new Audio('/assets/sounds/alarm.mp3');
  private globalSub: Subscription;

  constructor(private globalService: GlobalService) { }

  ngOnInit() {
    this.globalSub = this.globalService.getCommandsStream().subscribe((command: Command) => {
      // switch (command.type) {
      //   case 'start':
      //     this.start();
      //     break;
      //   case 'stop':
      //     this.stop();
      //     break;
      //   default:
      // }
    });
  }

  ngOnDestroy() {
    this.stop();
    this.globalSub.unsubscribe();
  }

  ngAfterViewInit() {
    const pickerH =  new Picker(this.picker.nativeElement, {
      container: '.js-mini-picker-container-countdown',
      inline: true,
      rows: 1,
      format: 'HH:mm:ss'
    });
    pickerH.setDate(new Date(1970, 0, 0, 0, 3));
  }

  public dateChanged(event) {
    const time = event.target.value.split(':');
    this.desiredTime.h = time[0];
    this.desiredTime.m = time[1];
    this.desiredTime.s = time[2];
    console.log(event.target.value);
  }

  public start() {
    console.log('start');
    this.endTime = Utils.addTime(this.desiredTime.h, this.desiredTime.m, this.desiredTime.s) - this.overallTime;
    this.stop();
    this.startTime = this.endTime;
    this.startInterval();
  }

  private startInterval() {
    this.intervalID = setInterval(() => {
      const diff = this.startTime - Date.now();
      if (diff < 0) {
        this.onTimeArrived();
        return;
      }
      this.time = this.format(this.overallTime + diff);
    }, 40);
  }

  public stop() {
    console.log('stop');
    clearInterval(this.intervalID);
    this.arrived = false;
    if (this.alarmSound) {
      this.alarmSound.pause();
      this.alarmSound.currentTime = 0;
    }
    this.intervalID = null;
    this.startTime = null;
    this.overallTime = 0;
  }

  public pause() {
    this.overallTime += Utils.addTime(this.desiredTime.h, this.desiredTime.m, this.desiredTime.s) - this.startTime;
    clearInterval(this.intervalID);
    this.intervalID = null;
  }

  private format(timestamp) {
    return Utils.formatDate('HH:mm:ss.SS', timestamp);
  }

  private onTimeArrived() {
    this.stop();
    this.arrived = true;
    this.alarmSound.loop = true;
    this.alarmSound.play();
  }
}
