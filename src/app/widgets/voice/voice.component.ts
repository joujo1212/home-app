import {Component, NgZone, OnDestroy, OnInit} from '@angular/core';
import {SpeechService} from '../../services/speech.service';
import {WeatherService} from '../../services/weather.service';
import {GlobalService} from '../../services/global.service';
import {Subscription} from 'rxjs';
import Command from '../../models/command.model';
import PNotify from 'pnotify/dist/es/PNotify';

@Component({
  selector: 'app-voice',
  templateUrl: './voice.component.html',
  styleUrls: ['./voice.component.css']
})
export class VoiceComponent implements OnInit, OnDestroy {

  public items = [];
  public listening = false;
  private lastText = '';
  private name = 'dom';
  private listenSub: Subscription;
  private isListeningSub: Subscription;
  // if true, listen on all commands, if false, ignore almost all commands (except getting name and say name)
  private directMode = false;
  commands = [];

  constructor(private speechService: SpeechService, private ngZone: NgZone, private weatherService: WeatherService, private globalService: GlobalService) {
    // PNotifyButtons; // Initiate the module. Important!
  }

  ngOnInit() {
    this.isListeningSub = this.speechService.getIsListening().subscribe(isListening => {
      this.ngZone.run(() => {

        this.listening = isListening;
      });
    });

    this.defineCommands();
  }

  ngOnDestroy() {
    this.isListeningSub.unsubscribe();
  }

  public listen() {
    this.listenSub = this.speechService.listen(true).subscribe((item: string) => {
      this.ngZone.run(() => {

        const command = item.replace(this.lastText, '');
        console.log('command', command);
        // this.speechService.speak('Ako si želáš');

        const dict = this.speechService.processInput(command);
        console.log('command', dict);


        this.commandsExecutor(dict, item, command);

      });
    });
  }

  defineCommands() {
    this.commands.push(new CommandBuilder()
      .hasOneOf([this.name])
      .disableDirectMode()
      .job(() => {
        this.speechService.speak('Želáte si?');
        this.directMode = true;
      }));
    this.commands.push(new CommandBuilder()
      .hasOneOf(['počasie', 'vonku'])
      .job(() => this.globalService.pushCommand(new Command('actual_weather'))));

    this.commands.push(new CommandBuilder()
      .hasOneOf(['predpoveď'])
      .job(() => this.globalService.pushCommand(new Command('forecast_weather'))));
    this.commands.push(new CommandBuilder()
      .hasOneOf(['ďakujem'])
      .job(() => {
        const greetings = ['Rado sa stalo', 'Zamálo', 'Aj nabudúce'];
        this.speechService.speak(greetings[this.randomIntFromInterval(0, greetings.length)]);
        this.directMode = false;
      }));
    this.commands.push(new CommandBuilder()
      .hasAll(['nové', 'meno'])
      .disableDirectMode()
      .job(() => {
        this.speechService.speak('Ako sa budem volať?').then(() => {
          this.listenSub.unsubscribe();
          this.speechService.stopListening();
          this.speechService.listen().subscribe((name: string) => {
            this.speechService.speak('Odteraz sa volám ' + name).then();
            console.log(name);
            this.name = name.toLowerCase();
            // update first command - switching to directMode should listen on new name
            this.commands[0].oneOf = [this.name];
            this.speechService.stopListening();
            setTimeout(() => {
              this.listen();
            }, 500);
          });
        });
      }));
    this.commands.push(new CommandBuilder()
      .hasAll(['ako', 'voláš'])
      .disableDirectMode()
      .job(() => {
        this.speechService.speak('Volám sa ' + this.name);
      }));
    this.commands.push(new CommandBuilder()
      .hasAll(['pridať', 'zoznam'])
      .job(() => {
        this.speechService.speak('Počúvam').then(() => {
          this.listenSub.unsubscribe();
          this.speechService.stopListening();
          this.speechService.listen().subscribe((name: string) => {
            this.globalService.pushCommand(new Command('add_shopping_list', name));
            console.log(name);
            this.speechService.stopListening();
            setTimeout(() => {
              this.listen();
            }, 1000);
          });
        });
      }));
    this.commands.push(new CommandBuilder()
      .hasAll(['nákupný', 'zoznam'])
      .job(() => {
        this.globalService.pushCommand(new Command('say_shopping_list'));
      }));
    this.commands.push(new CommandBuilder()
      .hasOneOf(['štart', 'start'])
      .job(() => {
        this.globalService.pushCommand(new Command('start'));
      }));
    this.commands.push(new CommandBuilder()
      .hasOneOf(['stop', 'start-stop'])
      .job(() => {
        this.globalService.pushCommand(new Command('stop'));
      }));
  }

  commandsExecutor(dict, item, command) {
    for (let i = 0; i < this.commands.length; i++) {
      const c = this.commands[i];
      if (!this.directMode && c.directModeOnly) {
        continue;
      }
      c.execute(dict);
      if (c.passTest) {
        this.lastText = item;
        // this.items.push(command);
        this.showNotif(command);
        break;
      }
    }
  }

  randomIntFromInterval(min: number, max: number): number {
    return Math.floor(Math.random() * (max - min + 1) + min);
  }

  private showNotif(title: string) {
    PNotify.defaults.delay = 1000;

    if (typeof (<any>window).stackBottomLeft === 'undefined') {
      (<any>window).stackBottomLeft = {
        'dir1': 'up',
        'firstpos1': 25,
        'push': 'top'
      };
    }
    PNotify.notice({
      title,
      type: 'info',
      width: '16vw',
      icon: 'fa fa-microphone',
      styling: {},
      addClass: 'custom nonblock',

      // text: 'My buttons are really lonely, so they\'re gonna hang out with us.',
      modules: {
        Animate: {
          animate: true,
          inClass: 'fadeInUp',
          outClass: 'fadeOutUp'
        }
      },
      stack: (<any>window).stackBottomLeft

    });
  }
}

class CommandBuilder {
  dict;
  task;
  passTest = true;
  oneOf = [];
  allOf = [];
  directModeOnly = true;

  /**
   * Enable listening all time, not only during direct mode
   * @returns {this}
   */
  public disableDirectMode() {
    this.directModeOnly = false;
    return this;
  }

  public hasAll(keywords: Array<string>) {
    if (0 === keywords.length) {
      return this;
    }
    this.allOf = this.allOf.concat(keywords);
    return this;
  }

  public hasOneOf(keywords: Array<string>) {
    if (0 === keywords.length) {
      return this;
    }
    this.oneOf = this.oneOf.concat(keywords);
    return this;
  }

  public job(task) {
    this.task = task;
    return this;
  }

  public test() {
    this.passTest = true;
    if (this.allOf.length > 0 && !this.allOf.every(value => this.dict.indexOf(value) >= 0)) {
      this.passTest = false;
    }

    if (this.oneOf.length > 0 && !this.oneOf.some(value => this.dict.indexOf(value) >= 0)) {
      this.passTest = false;
    }
  }

  public execute(dict) {
    this.dict = dict;
    this.test();
    if (this.passTest) {
      this.task();
    }
  }

  public passed() {
    return this.passTest;
  }
}
