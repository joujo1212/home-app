import {AfterViewInit, Component, OnDestroy, OnInit, ViewChild} from '@angular/core';
import {GlobalService} from '../../services/global.service';
import Command from '../../models/command.model';
import {Utils} from '../../utils';
import Picker from 'pickerjs/dist/picker.esm';
import {Subscription} from 'rxjs';

@Component({
  selector: 'app-alarm',
  templateUrl: './alarm.component.html',
  styleUrls: ['./alarm.component.scss']
})
export class AlarmComponent implements OnInit, OnDestroy, AfterViewInit {
  @ViewChild('picker') picker;
  public intervalID;
  public time = '00:00';
  public desiredTime: Date;
  // true if time arrived
  public arrived = false;
  private alarmSound = new Audio('/assets/sounds/alarm.mp3');
  public isOn = false;
  private globalSub: Subscription;
  public started = false;

  constructor(private globalService: GlobalService) { }

  ngOnInit() {
    this.globalSub = this.globalService.getCommandsStream().subscribe((command: Command) => {
      // switch (command.type) {
      //   case 'start':
      //     this.start();
      //     break;
      //   case 'stop':
      //     this.stop();
      //     break;
      //   default:
      // }
    });
  }

  ngOnDestroy() {
    this.stop();
    this.globalSub.unsubscribe();
  }

  ngAfterViewInit() {
    const pickerH =  new Picker(this.picker.nativeElement, {
      container: '.js-mini-picker-container-alarm',
      inline: true,
      rows: 1,
      format: 'HH:mm'
    });
    pickerH.setDate(new Date());
  }

  public dateChanged(event) {
    const time = event.target.value.split(':');
    this.desiredTime =  Utils.parseDateFromString(`${time[0]}:${time[1]}`, 'HH:mm');
  }

  public start() {
    this.stop();
    if (!this.desiredTime) {
      this.desiredTime = new Date();
    }
    if (this.desiredTime.getTime() < Date.now()) {
      this.desiredTime = Utils.addDay(1, this.desiredTime.getTime());
    }
    console.log('start');
    this.isOn = true;
    this.startInterval();
    this.started = true;
  }

  private startInterval() {
    this.intervalID = setInterval(() => {
      const diff = this.desiredTime.getTime() - Date.now();
      if (diff < 0) {
        this.onTimeArrived();
        return;
      }
      this.time = this.format(diff);
    }, 1000);
  }

  public stop() {
    console.log('stop');
    this.started = false;
    clearInterval(this.intervalID);
    this.arrived = false;
    if (this.alarmSound) {
      this.alarmSound.pause();
      this.alarmSound.currentTime = 0;
    }
    this.intervalID = null;
    this.isOn = false;
  }

  private format(timestamp) {
    return Utils.formatDate('HH:mm:ss', timestamp);
  }

  private onTimeArrived() {
    this.stop();
    this.arrived = true;
    this.alarmSound.loop = true;
    this.alarmSound.play();
  }

  public isOnChanged(event) {
    this.isOn = event.target.checked;
    if (this.isOn) {
      this.start();
    } else {
      this.stop();
    }
  }

}
