import {Component, ElementRef, OnInit, ViewChild} from '@angular/core';
import {WeatherService} from '../../services/weather.service';
import WeatherForecast from '../../models/weather-forecast.model';
import {GlobalService} from '../../services/global.service';
import {SpeechService} from '../../services/speech.service';
import Command from '../../models/command.model';
import ShoppingListItem from '../../models/shopping-list-item.model';

@Component({
  selector: 'app-shopping-list',
  templateUrl: './shopping-list.component.html',
  styleUrls: ['./shopping-list.component.scss']
})
export class ShoppingListComponent implements OnInit {
  @ViewChild('newItemInput') newItemInput: ElementRef;

  public shoppingList: Array<ShoppingListItem> = [
    {name: 'káva', done: false},
    {name: 'mlieko', done: false},
    {name: 'chlieb', done: false},
  ];

  public isAddItemShown = false;
  public newItem = '';

  constructor(private weatherService: WeatherService, private speechService: SpeechService, private globalService: GlobalService) {
  }

  ngOnInit() {
    this.globalService.getCommandsStream().subscribe((command: Command) => {
      switch (command.type) {
        case 'say_shopping_list':
          this.handleSayShoppingList();
          break;
        case 'add_shopping_list':
          this.handleAddShoppingList(command.args);
          break;
        default:
      }
    });
  }

  private handleSayShoppingList() {
    this.speechService.speak(`Na zozname máte: ${this.shoppingList.map(item => item.name).join(', ')}.`);
  }

  private handleAddShoppingList(item: any) {
    console.log('pridavam na zoznam ' + item);
    const items = item.split(' a ').map(i => ({name: i, done: false}));
    this.shoppingList = this.shoppingList.concat(items);
    this.speechService.speak(`Položka ${item} pridaná na nákupný zoznam`).then();
  }

  public toggleItem(item: ShoppingListItem) {
    item.done = !!item.done;
  }

  public showAddItem() {
    this.isAddItemShown = true;
    setTimeout(() => {
      this.newItemInput.nativeElement.focus();
    }, 100);
  }

  public hideAddItem() {
    this.isAddItemShown = false;
  }

  public addItem() {
    this.shoppingList.push({name: this.newItem, done: false});
    this.isAddItemShown = false;
    this.newItem = '';
  }

  public removeItem(item) {
    this.shoppingList.splice(this.shoppingList.indexOf(item), 1);
  }
}
