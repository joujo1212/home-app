import {Component, Input, OnInit, ViewChild} from '@angular/core';
import {GlobalService} from '../../services/global.service';
import {WidgetService} from '../../services/widget.service';
import {MetaService} from '../../services/meta.service';

@Component({
  selector: 'app-widget',
  templateUrl: './widget.component.html',
  styleUrls: ['./widget.component.scss']
})
export class WidgetComponent implements OnInit {
  @ViewChild('container') container;
  @Input('name') name: string;
  @Input('transparent') transparent: boolean;
  private onClickBound;
  private widgets;
  public widgetMeta = {width: 1, height: 1, square: false};
  public isSettingsShown = false;
  public meta;

  constructor(private globalService: GlobalService, private widgetService: WidgetService, private metaService: MetaService) {
  }

  ngOnInit() {
    this.widgetService.getWidgets().subscribe(widgets => {
      this.widgets = widgets;
      this.widgetMeta = this.widgets[this.name] || {};
    });
   this.metaService.getMeta().subscribe(meta => {
     this.meta = meta;
     console.log(meta);
   });
  }

  private offClickHandler(event:any) {
    if (!this.container.nativeElement.contains(event.target)) { // check click origin
      this.isSettingsShown = false;
      this.removeClickListener();
    }
  }

  public toggleSettings() {
    if (!this.onClickBound) {
      this.onClickBound = this.offClickHandler.bind(this);
    }
    this.isSettingsShown = !this.isSettingsShown;
    if (this.isSettingsShown) {
      this.addClickListener();
    } else {
      this.removeClickListener();
    }
  }

  private addClickListener() {
    document.addEventListener('click', this.onClickBound); // bind on doc
  }

  private removeClickListener() {
    document.removeEventListener('click', this.onClickBound);
  }

  public incrementWidth() {
    if (this.widgetMeta.width === 5) { return; }
    this.widgetMeta.width += 1;
    this.widgetService.setWidgets(this.widgets);
  }

  public decrementWidth() {
    if (this.widgetMeta.width === 1) { return; }
    this.widgetMeta.width -= 1;
    this.widgetService.setWidgets(this.widgets);
  }

  public incrementHeight() {
    if (this.widgetMeta.height === 5) { return; }
    this.widgetMeta.height += 1;
    this.widgetService.setWidgets(this.widgets);
  }

  public decrementHeight () {
    if (this.widgetMeta.height === 1) { return; }
    this.widgetMeta.height -= 1;
    this.widgetService.setWidgets(this.widgets);
  }

  public toggleSquare() {
    this.widgetMeta.square = !this.widgetMeta.square;
    this.widgetService.setWidgets(this.widgets);
  }
}
