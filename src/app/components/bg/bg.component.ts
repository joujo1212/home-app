import {Component, OnDestroy, OnInit} from '@angular/core';
import {Subscription} from 'rxjs';
import {GlobalService} from '../../services/global.service';
import {MetaService} from '../../services/meta.service';
import Meta from '../../models/meta.model';
import Background from '../../models/background.model';
import SunCalc from 'suncalc';

@Component({
  selector: 'app-bg',
  templateUrl: './bg.component.html',
  styleUrls: ['./bg.component.scss']
})
export class BgComponent implements OnInit, OnDestroy {

  public bg: Background;
  // image for day/night depends on actual time
  public image: string;
  private bgSub: Subscription;
  private intervalID;

  constructor(private globalService: GlobalService, private metaService: MetaService) {
  }

  ngOnInit() {
    this.bgSub = this.metaService.getMeta().subscribe((meta: Meta) => {
      if (!meta.bg) { return; }
      this.bg = meta.bg;
      clearInterval(this.intervalID);
      this.setImage();
      this.intervalID = setInterval(() => {
        this.setImage();
      }, 1000 * 60);
    });
  }

  ngOnDestroy() {
    if (this.bgSub) {
      this.bgSub.unsubscribe();
    }
    clearInterval(this.intervalID);
  }

  private setImage() {
    if (!this.bg) { return; }
    const now = Date.now();
    const times = SunCalc.getTimes(/*Date*/ new Date(), /*Number*/  48.71, /*Number*/ 21.26);
    const sunrise = times.sunrise.getTime();
    const sunset = times.sunset.getTime();
    if (now < sunset && now > sunrise) {
      this.image = this.bg.day;
      console.log('day');
    } else {
      this.image = this.bg.night;
      console.log('night');
    }
    console.log(sunrise, sunset);
  }
}
