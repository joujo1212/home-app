import {Component, OnDestroy, OnInit, ViewChild} from '@angular/core';
import {GlobalService} from '../../services/global.service';
import {Subscription} from 'rxjs';
import {Utils} from '../../utils';
import {WidgetService} from '../../services/widget.service';
import {DisplayGrid, GridsterConfig, GridsterItem} from 'angular-gridster2';
import {VoiceComponent} from '../../widgets/voice/voice.component';
import {ForecastComponent} from '../../widgets/forecast/forecast.component';
import {WeatherComponent} from '../../widgets/weather/weather.component';
import {ShoppingListComponent} from '../../widgets/shopping-list/shopping-list.component';
import {StopwatchComponent} from '../../widgets/stopwatch/stopwatch.component';
import {CountdownComponent} from '../../widgets/countdown/countdown.component';
import {ClockComponent} from '../../widgets/clock/clock.component';
import {AlarmComponent} from '../../widgets/alarm/alarm.component';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss']
})
export class DashboardComponent implements OnInit, OnDestroy {
  @ViewChild('grid') grid;
  private isFullscreen = false;
  private globalSub: Subscription;
  public widgets: Array<GridsterItem> = [];
  public draggable = false;
  public options: GridsterConfig;
  public components = {
    forecast: ForecastComponent,
    weather: WeatherComponent,
    voice: VoiceComponent,
    shoppingList: ShoppingListComponent,
    stopwatch: StopwatchComponent,
    countdown: CountdownComponent,
    clock: ClockComponent,
    alarm: AlarmComponent
  };
  public theme = 'theme-red';

  constructor(private globalService: GlobalService, private widgetService: WidgetService) {
  }

  ngOnInit() {
    this.options = {
      itemChangeCallback: this.itemChange.bind(this),
      itemResizeCallback: this.itemResize.bind(this),
      draggable: {
        enabled: false
      },
      resizable: {
        enabled: false
      },
      minCols: 12,
      maxCols: 12,
      minRows: 12,
      maxRows: 12,
      displayGrid: DisplayGrid.None
    };

    this.globalSub = this.widgetService.getWidgets().subscribe(widgets => {
      this.widgets = widgets.filter(widget => widget.visible);
      console.log('widgets', this.widgets);
    });

    document.onwebkitfullscreenchange = event => {
      console.log('EVENT', event);
      this.isFullscreen = !this.isFullscreen;
    };
  }

  private itemChange(item, itemComponent) {
    console.info('itemChanged', item, itemComponent);
    this.widgetService.updateWidget(item.id, item);

  }

  private itemResize(item, itemComponent) {
    console.info('itemResized', item, itemComponent);
    this.widgetService.updateWidget(item.id, item);
  }

  ngOnDestroy() {
    if (this.globalSub) {
      this.globalSub.unsubscribe();
    }
  }

  public toggleDraggie() {
    this.options.draggable.enabled = !this.options.draggable.enabled;
    this.options.resizable.enabled = !this.options.resizable.enabled;
    // this.widgets[0].dragEnabled = false;
    console.log(this.options);
    this.changedOptions();
  }

  changedOptions() {
    if (this.options.api && this.options.api.optionsChanged) {
      this.options.api.optionsChanged();
    }
  }

  public toggleFullscreen() {
    if (this.isFullscreen) {
      Utils.closeFullscreen();
    } else {
      Utils.openFullscreen();
    }
  }

  public toggleSidebar() {
    this.globalService.setSideNavState(true);
  }
}
