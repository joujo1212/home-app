import {Component, OnInit, ViewChild} from '@angular/core';
import M from 'materialize-css/dist/js/materialize';
import {GlobalService} from '../../services/global.service';
import {MetaService} from '../../services/meta.service';
import Background from '../../models/background.model';
import {Observable, Subscription} from 'rxjs';
import Widget from '../../models/widget.model';
import {WidgetService} from '../../services/widget.service';

@Component({
  selector: 'app-sidenav',
  templateUrl: './sidenav.component.html',
  styleUrls: ['./sidenav.component.scss']
})
export class SidenavComponent implements OnInit {
  @ViewChild('tabs') tabsElem;
  private state;
  private instance;
  public images: Background[] = [
    {day: 'http://www.aljanh.net/data/archive/img/2935519721.jpeg', night: 'http://www.aljanh.net/data/archive/img/1567150245.jpeg', name: 'Les'},
    {day: 'http://www.aljanh.net/data/archive/img/2461018217.jpeg', night: 'http://www.aljanh.net/data/archive/img/3373129059.jpeg', name: 'Mesto'},
    {day: 'http://www.aljanh.net/data/archive/img/1040897943.jpeg', night: 'http://www.aljanh.net/data/archive/img/3370080883.jpeg', name: 'Abstrakt'},
    {day: 'http://www.aljanh.net/data/archive/img/993507161.jpeg', night: 'http://www.aljanh.net/data/archive/img/83583668.jpeg', name: 'Lúka'},
    {day: 'http://www.aljanh.net/data/archive/img/1257235202.jpeg', night: 'http://www.aljanh.net/data/archive/img/1471160237.jpeg', name: 'Detail'},
    {day: 'http://www.aljanh.net/data/archive/img/382835804.jpeg', night: 'http://www.aljanh.net/data/archive/img/2074730086.jpeg', name: 'Detail 2'}
  ];
  public colors = [
    '230, 79, 79',
    '230, 191, 79',
    '114, 191, 44',
    '44, 191, 191',
    '94, 44, 191',
    '191, 44, 159',
    '0, 0, 0',
    '49, 56, 70'
  ];
  public meta: any;
  public widgets: Observable<Widget[]>;

  constructor(private globalService: GlobalService, private metaService: MetaService, private widgetService: WidgetService) { }

  ngOnInit() {
    document.addEventListener('DOMContentLoaded', () => {
      const elems = document.querySelectorAll('.sidenav')[0];
      const instances = M.Sidenav.init(elems, {draggable: false});
      this.instance = M.Sidenav.getInstance(elems);

    });
    this.globalService.getSideNavState().subscribe(state => {
      this.state = state;
      if (!this.instance) { return; }
      if (this.state) {
        this.instance.open();
      } else {
        this.instance.close();
      }
    });
    this.metaService.getMeta().subscribe(meta => {
      this.meta = meta;
    });
    this.widgets = this.widgetService.getWidgets();
  }

  public changeBg(bg: string) {
    this.meta.bg = bg;
    this.metaService.setMeta(this.meta);
  }

  public opacityChanged(event) {
    this.metaService.setMeta(this.meta);
  }

  public changeColor(color) {
    this.meta.color = color;
    this.metaService.setMeta(this.meta);
  }

  public toggleWidget(widget: Widget) {
    this.widgetService.updateWidget(widget.id, {visible: !widget.visible});
  }
}
